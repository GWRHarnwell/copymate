using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace copymate
{
    public partial class Form1 : Form
    {
        int _elements;
        string _filename;
        string _currentSelected = "";

        public Form1()
        {
            InitializeComponent();
            _elements = 50;
            _filename = String.Format(@"{0}", Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments), "items.txt"));

            newEntryTxt.LostFocus += new EventHandler(delegate(object sender, EventArgs e)
            {
                newEntryTxt.Focus();
            });
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            this.TopMost = true;
            Rectangle workingArea = Screen.GetWorkingArea(this);
            this.Location = new Point(workingArea.Right - Size.Width,
                                      workingArea.Bottom - Size.Height);

            var addBtn = Controls.Find("addBtn", true).FirstOrDefault();
            this.AcceptButton = addBtn as Button;
            Generate();
        }

        private void Generate()
        {

            String list = ReadFromFile();
            var items = list.Split(',');
            foreach (var i in items)
            {
                if (String.IsNullOrEmpty(i))
                    continue;
                var identifier = Guid.NewGuid();
                CreateButton("btn_" + identifier, i, 200, 20);
                CreateButton("removeBtn_" + identifier, "X", 30, 230);
                _elements += 35;
            }
        }

        private String ReadFromFile()
        {
            try
            {
                String list = "";
                using (StreamReader r = new StreamReader(_filename))
                {
                    list = r.ReadToEnd();
                }
                return list;
            }
            catch (FileNotFoundException ex)
            {
                var newFile = File.Create(_filename);
                newFile.Close();
                using (StreamWriter writer = new StreamWriter(_filename, true))
                {
                    writer.Write("Hello world" + ",");
                }
                Generate();
            }
            return "";
        }

        private void CreateButton(String id, String text, int width, int xOffset)
        {
            var btn = new Button();
            btn.Name = id;
            btn.Width = width;
            btn.Height = 30;
            btn.Text = text;
            btn.Location = new Point(xOffset, _elements);

            btn.Click += new System.EventHandler(this.customBtn_Click);
            this.Controls.Add(btn);
        }

        private void addBtn_Click(object sender, EventArgs e)
        {
            if (String.IsNullOrEmpty(this.newEntryTxt.Text))
                return;
            var text = this.newEntryTxt.Text;
            this.newEntryTxt.Text = "";

            var identifier = Guid.NewGuid();
            CreateButton(String.Concat("btn_", identifier), text, 200, 20);
            CreateButton(String.Concat("removeBtn_", identifier), "X", 30, 230);
            _elements += 35;

            //Add to txt file
            using (StreamWriter writer = new StreamWriter(_filename, true))
            {
                writer.Write(text + ",");
            }

        }

        private void customBtn_Click(object sender, EventArgs e)
        {
            Button button = sender as Button;
            if (button != null)
            {
                var id = button.Name;
                if (!String.IsNullOrEmpty(id) && id.Contains("removeBtn_"))
                {
                    var idParts = id.Split('_');
                    idParts[0] = "";
                    var identifier = String.Join("_", idParts);

                    //Remove btn and delte btn
                    Controls.Remove(button);
                    var btn = Controls.Find("btn" + identifier, true).FirstOrDefault();
                    if (btn != null)
                    {
                        //Delete from store
                        var items = ReadFromFile().Split(',');
                        var newList = "";
                        foreach (var i in items)
                        {
                            if (i.ToUpperInvariant() != btn.Text.ToUpperInvariant())
                                newList += i + ",";
                        }

                        //Add to txt file
                        using (StreamWriter writer = new StreamWriter(_filename, false))
                        {
                            writer.Write(newList);
                        }
                        Controls.Remove(btn);

                    }

                }
                else
                {
                    if (!String.IsNullOrEmpty(_currentSelected))
                    {
                        var lastBtn = Controls.Find(_currentSelected, true).FirstOrDefault();
                        lastBtn.ForeColor = System.Drawing.Color.Black;
                    }

                    button.ForeColor = System.Drawing.Color.Green;
                    _currentSelected = button.Name;
                    //Set to clipboard
                    Clipboard.SetText(button.Text);
                }
            }
        }

        private void newEntryTxt_TextChanged(object sender, EventArgs e)
        {

        }

    }
}
